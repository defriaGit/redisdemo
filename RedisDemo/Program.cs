﻿using System;
using System.Linq;
using StackExchange.Redis;

namespace RedisDemo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var redisServer = "localhost:6379";
            var redisConnection = ConnectionMultiplexer.Connect(redisServer);
            var endPoints = redisConnection.GetEndPoints();
            var server = redisConnection.GetServer(endPoints[0]);
            var db = redisConnection.GetDatabase();
            var keys = server.Keys();

            foreach (var redisKey in keys) Console.WriteLine($"{redisKey}");


            Console.WriteLine();
            Console.WriteLine("*********** REDIS LIST EXAMPLE ***********");
            var friends = db.ListRange("friends");
            foreach (var friend in friends) Console.WriteLine($"Friend Name: {friend}");

            Console.WriteLine();
            Console.WriteLine("*********** REDIS LIST EXAMPLE *************");
            var tasks = db.ListRange("tasks", 0, -1);
            foreach (var task in tasks) Console.WriteLine($"Task: {task}");

            Console.WriteLine();
            Console.WriteLine("******** REDIS HASK KEY EXMAPLE **********");
            var hashKey = server.Keys(pattern: "call");
            var hashKeyResult = db.HashGetAll(hashKey.Single());
            foreach (var hashEntry in hashKeyResult) Console.WriteLine($"{hashEntry.Name}: {hashEntry.Value}");


            var call = new[]
            {
                new HashEntry("name", "C#"),
                new HashEntry("company", "FitnessFam"),
                new HashEntry("phone", "987456321"),
                new HashEntry("time", "Saturday at 11 am")
            };
            db.HashSet("call", call);
        }
    }
}