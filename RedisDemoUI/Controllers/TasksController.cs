﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RedisDemoUI.Models;
using StackExchange.Redis;
using Task = RedisDemoUI.Models.Task;

namespace RedisDemoUI.Controllers
{
    public class TasksController : Controller
    {
        private readonly ApplicationDbContext _db;
        private const string Tasks = "tasks";
        private const string NextBusinessCall = "call";

        public TasksController()
        {
            _db = new ApplicationDbContext();
        }
        public IActionResult Index()
        {
           
            var values = _db.Database.ListRange(Tasks).ToArray();
            var call = _db.Database.HashGetAll(NextBusinessCall);
            var taskViewModel = new TaskViewModel(call);
            foreach (var value in values)
            {
                var task = new Task {Name = value};
                taskViewModel.Tasks.Add(task);

            }

            return View(taskViewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Task task)
        {
            if (ModelState.IsValid)
            {
                await _db.Database.ListRightPushAsync(Tasks, task.Name);
                return RedirectToAction(nameof(Index));
            }

            return View(task);
        }

        public async Task<IActionResult> Details(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return NotFound();
            }

            var results = await _db.Database.ListRangeAsync(Tasks);

            if (results.All(i => i != name))
            {
                return NotFound();
            }

            var task = new Task()
            {
                Name = results.Single(i => i == name)
            };
             
            return View(task);
        }
        public async Task<IActionResult> Edit(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return NotFound();
            }

            var results = await _db.Database.ListRangeAsync(Tasks);

            if (results.All(i => i != name))
            {
                return NotFound();
            }

            var task = new Task()
            {
                Name = results.Single(i => i == name)
            };
             
            return View(task);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string name, Task task)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var taskItem = await _db.Database.ListRangeAsync(Tasks, 1);
                task.Name = taskItem.First();

                return RedirectToAction(nameof(Index));
            }

            return View(task);
        }
        public async Task<IActionResult> Delete(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return NotFound();
            }

            var results = await _db.Database.ListRangeAsync(Tasks);

            if (results.All(i => i != name))
            {
                return NotFound();
            }

            var task = new Task()
            {
                Name = results.Single(i => i == name)
            };
             
            return View(task);
        }

        public IActionResult EditCall()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCall(string name, NextBusinessCall nextCall)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return NotFound();
            }

            var call = new[]
            {
                new HashEntry("name", $"{nextCall.Name}"),
                new HashEntry("company", $"{nextCall.Company}"),
                new HashEntry("phone", $"{nextCall.Phone}"),
                new HashEntry("time", $"{nextCall.Time}")
            };
            await _db.Database.HashSetAsync("call", call);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveTask(string name)
        {
            await _db.Database.ListRemoveAsync(Tasks, name);
            return RedirectToAction(nameof(Index));
        }
    }
}