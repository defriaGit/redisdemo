﻿using System;
using System.Collections.Generic;
using System.Net;
using StackExchange.Redis;


namespace RedisDemoUI.Models
{
    public class ApplicationDbContext
    {
        private static string RedisServer => "localhost:6379";
        private static readonly ConnectionMultiplexer Connection = ConnectionMultiplexer.Connect(RedisServer);
        private static readonly EndPoint[] EndPoints = Connection.GetEndPoints();
        private static readonly IServer Server = Connection.GetServer(EndPoints[0]);
        public IDatabase Database = Connection.GetDatabase();
        public IEnumerable<RedisKey> Keys = Server.Keys();
    }
}