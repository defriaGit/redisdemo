﻿namespace RedisDemoUI.Models
{
    public class NextBusinessCall
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Time { get; set; }
    }
}