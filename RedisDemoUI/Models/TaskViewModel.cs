﻿using System.Collections.Generic;
using StackExchange.Redis;

namespace RedisDemoUI.Models
{
    public static class CustomExtensions
    {
        public static string ToUpperFirstLetter(this string stringValue)
        {
            var stringValueArray = stringValue.ToCharArray(0, stringValue.Length);
            var UpperFirstLetter = stringValueArray[0].ToString().ToUpper();
            return stringValue.Replace(stringValueArray[0].ToString(), UpperFirstLetter); ;

        }
    }
    public class TaskViewModel
    {
        public TaskViewModel(HashEntry[] call)
        {
            Call = call;
            Tasks = new List<Task>();
            NextBusinessCall = new List<string>();
            PopulateNextBusinesCall();
        }
        public List<Task> Tasks { get; set; }
        private HashEntry[] Call { get; set; }

        public string CallHaskKeyValue => "call";

        public List<string> NextBusinessCall { get; private set; }

        private void PopulateNextBusinesCall()
        {
            foreach (var entry in Call)
            {
                string call = $"{entry.Name.ToString().ToUpperFirstLetter()}: {entry.Value}";
                NextBusinessCall.Add(call);
            }
        }

        
    }
}